package edu.nf.ch04.controller;

import edu.nf.ch04.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * @Date 2023-10-20
 * @Author qiu
 * 第四章：数据绑定
 */
@Controller
@Slf4j
public class UserController {

    /**
     * 注册自定义转换器，@InitBinder 注解标注的方法会在执行
     * 任何 controller 的方法之前先执行，spring 会传入
     * 一个 webBinder 的参数，使用这个参数可以注册任意的 Formatter
     * @param binder 数据绑定器,用于注册各种格式化类
     */
    @InitBinder
    public void regFormatter(WebDataBinder binder){
        binder.addCustomFormatter(new DateFormatter("yyyy-MM-dd"));
    }

    @PostMapping("/add")
    public ModelAndView add(HttpServletRequest request){
        String userName = request.getParameter("username");
        String age = request.getParameter("age");
        log.info(userName + "  "+ age);

        // 将参数当如请求作用域中
//        request.setAttribute("username",userName);
//        request.setAttribute("age",age);

        // 通过 modelAndView 将参数放入请求作用域
        ModelAndView index = new ModelAndView("index");
        index.addObject("username",userName);
        index.addObject("age",age);

        return index;
    }

    /**
     * 将请求数据直接绑定到参数上，
     * 默认参数名与请求中的 name 保持一致即可映射
     * 否则使用 @RequestParam 注解来
     *
     * required = true:必须提交
     * defaultValue = "aaaa"：设置默认值
     * @param userName
     * @param userAge
     * @return
     */
    @PostMapping("/add2")
    public ModelAndView add2(@RequestParam(value = "username" ,required = true,defaultValue = "aaaa") String userName , @RequestParam("age") Integer userAge,
                             String[] tel, Date birth
                             ){
        // 通过 modelAndView 将参数放入请求作用域
        ModelAndView index = new ModelAndView("index");
        // 将参数保存到请求作用域
        index.addObject("username",userName);
        index.addObject("age",userAge);
        index.addObject("tel1",tel[0]);
        index.addObject("tel2",tel[1]);
        index.addObject("birth",birth);

        return index;
    }


    @PostMapping("/add3")
    public ModelAndView add3(User user){
        // 通过 modelAndView 将参数放入请求作用域
        ModelAndView index = new ModelAndView("index");
        // 将参数保存到请求作用域
        index.addObject("username",user.getUserName());
        index.addObject("age",user.getAge());
        index.addObject("tel1",user.getTel().get(0));
        index.addObject("tel2",user.getTel().get(1));
        index.addObject("birth",user.getBirth());
        index.addObject("cardNum",user.getCard().getCardNum());
        log.info(user.getCard().getCardNum() + "========");
        index.addObject("addr1",user.getAddresses().get(0).getAddr());
        index.addObject("addr2",user.getAddresses().get(1).getAddr());

        return index;
    }

    /**
     * 路径参数绑定
     * 请求格式：/url地址/(变量)
     * 并且使用 @PathVariable 注解
     * @param uid
     * @return
     */
    @GetMapping("/user/{id}")
    public ModelAndView getUser(@PathVariable("id") String uid){
        String aa = "";
        ModelAndView mav = new ModelAndView("index");
        mav.addObject("uid",uid);
        return mav;
    }

}
