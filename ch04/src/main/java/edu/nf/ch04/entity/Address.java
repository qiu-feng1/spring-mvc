package edu.nf.ch04.entity;

import lombok.Data;

/**
 * @Date 2023-10-20
 * @Author qiu
 */
@Data
public class Address {

    private String addr;

}
