package edu.nf.ch04.entity;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @Date 2023-10-20
 * @Author qiu
 * 映射到实体的时候，字段名与请求中的 name 保持一致
 *
 */
@Data
public class User {

    private String userName;
    private Integer age;
    private Date birth;
    private List<String> tel;
    // 一对一关联
    private Card card;
    // 一对多关联
    private List<Address> addresses;



}
