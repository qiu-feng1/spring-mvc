package edu.nf.ch08.factory;

import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.FactoryBean;



/**
 * @Date 2023-10-26
 * @Author qiu
 * 整合 Minio
 */
@Setter
@Slf4j
public class MinioFactoryBean implements FactoryBean<MinioClient> {

    /**
     * 注入相关的配置属性
     */
    private String url;
    private String user;
    private String password;
    private String bucket;

    @Override
    public MinioClient getObject() throws Exception {
        MinioClient minioClient = MinioClient.builder()
                .endpoint(url)
                .credentials(user,password)
                .build();
        // 初始化桶
        initBucket(minioClient);
        log.info("已初始化桶" + bucket);
        return minioClient;
    }

    private void initBucket(MinioClient client) throws Exception {
        if (!client.bucketExists(BucketExistsArgs.builder().bucket(bucket).build())){
            client.makeBucket(MakeBucketArgs.builder().bucket(bucket).build());
        }
    }

    @Override
    public Class<?> getObjectType() {
        return MinioClient.class;
    }
}
