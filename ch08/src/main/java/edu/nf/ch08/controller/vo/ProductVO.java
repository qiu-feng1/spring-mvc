package edu.nf.ch08.controller.vo;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Date 2023-10-24
 * @Author qiu
 * 商品 VO 对象，用于保存页面提交的数据
 * 后续将这个 vo 拷贝到 entity 中
 */
@Data
public class ProductVO {

    /**
     * 商品名称
     */
    private String productName;

    /**
     * 商品图片
     */
    private MultipartFile[] file;

}
