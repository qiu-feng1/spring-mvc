package edu.nf.ch08.controller;

import edu.nf.ch08.controller.vo.ResultVO;
import io.minio.GetObjectArgs;
import io.minio.ListObjectsArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.errors.*;
import io.minio.messages.Item;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Date 2023-10-26
 * @Author qiu
 */
@RestController
@RequiredArgsConstructor
@Slf4j
public class MinioController {

    //注入minioClient
    private final MinioClient minioClient;

    /**
     * path路径参数就是桶下面的子目录
     * @return
     */
    @PostMapping("/upload/{path}")
    public ResultVO upload(@PathVariable("path") String path,
                           MultipartFile[] files) throws Exception{
        //循环遍历上传的附件
        for(MultipartFile file : files) {
            //获取文件名
            String fileName = file.getOriginalFilename();
            //获取文件的输入流读取文件内容
            InputStream inputStream = file.getInputStream();
            //将文件上传到minio服务器
            minioClient.putObject(PutObjectArgs.builder()
                    .bucket("myapp")
                    //远程上传的路径
                    .object(path + "/" + fileName)
                    //设置一个输入流，-1表示读到文件的末尾
                    .stream(inputStream, file.getSize(),-1)
                    //文件的类型
                    .contentType(file.getContentType())
                    .build());
        }
        return new ResultVO();
    }

    /**
     * 文件下载
     * @param path 远程文件夹
     * @param fileName 文件名
     * @return
     */
    @GetMapping("/download/{path}/{fileName}")
    public ResponseEntity<InputStreamSource> download(
            @PathVariable("path") String path,
            @PathVariable("fileName") String fileName) throws Exception{
        //根据文件名从minio获取一个远程的输入流
        InputStream inputStream = minioClient.getObject(
                GetObjectArgs.builder()
                        .bucket("myapp")
                        .object(path + "/" + fileName)
                        .build());
        HttpHeaders headers = new HttpHeaders();
        fileName = URLEncoder.encode(fileName, "UTF-8");
        headers.setContentDispositionFormData("attachment",
                fileName);
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        InputStreamSource isr = new InputStreamResource(inputStream);
        return new ResponseEntity<>(isr, headers, HttpStatus.CREATED);

    }

    /**
     * 根据路径目录查询文件列表
     * @param path
     * @return
     */
    @GetMapping("/list/{path}")
    public ResultVO<List<String>> listFiles(@PathVariable("path") String path) {
//        List<String> list = new ArrayList<>();
        Map<String,Object> map = new HashMap<>();
        //使用minio获取列表
        minioClient.listObjects(ListObjectsArgs
                        .builder()
                        .bucket("myapp")
                        //要浏览的文件夹
                        .prefix(path + "/")
                        .build())
                //循环遍历集合，每一个itemResult都封装了一个文件信息
                .forEach(itemResult -> {
                    //获取文件名保存到list集合中
                    try {
                        Item item = itemResult.get();
                        //获取文件大小
                        //item.size();
                        //获取文件名
                        String fileName = item.objectName();
                        //去掉前缀（目录）
                        fileName = StringUtils.getFilename(fileName);
                        //获取文件大小
                        double fileSizeInKB = item.size() / 1024.0;
                        DecimalFormat df = new DecimalFormat("0.00");

                        String formattedNum = df.format(fileSizeInKB);
                        log.info("文件大小：" + formattedNum);
                        //加入到list集合中
                        map.put(fileName,formattedNum);
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                        throw new RuntimeException(e);
                    }
                });
        ResultVO vo = new ResultVO();
        vo.setData(map);
        return vo;
    }
}
