package edu.nf.ch08.controller;

import edu.nf.ch08.controller.vo.ResultVO;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.errors.*;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * @Date 2023-10-26
 * @Author qiu
 */
@Setter
@RequiredArgsConstructor
@Slf4j
public class DemoMinio {

    // 注入 minioClient
    private final MinioClient minioClient;

    /**
     * path 路径参数就是桶下面的子目录
     * @param path
     * @return
     */
    @PostMapping("/upload/{path}")
    public ResultVO upload(@PathVariable("path") String path, MultipartFile[] files) throws IOException, ServerException, InsufficientDataException, ErrorResponseException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
            // 循环遍历上传的附件
        for (MultipartFile file : files) {
            // 获取文件名
            String filename = file.getOriginalFilename();
            // 获取文件的输入流，读取文件内容
            InputStream inputStream = file.getInputStream();
            // 将文件上传到 minion 服务器
            minioClient.putObject(PutObjectArgs.builder()
                            // 桶的名称
                            .bucket("myapp")
                            //  远程上传的路径
                            .object(path + "/" + filename)
                            // 设置一个输入流，-1表示文件的末尾
                            .stream(inputStream,file.getSize(),-1)
                            .contentType(file.getContentType())
                    .build());

        }
        return new ResultVO();
    }

}
