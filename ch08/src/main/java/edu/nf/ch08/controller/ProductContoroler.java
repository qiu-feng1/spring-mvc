package edu.nf.ch08.controller;

import edu.nf.ch08.controller.vo.ProductVO;
import edu.nf.ch08.controller.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.URLEncoder;
import java.nio.file.FileSystems;
import java.nio.file.Path;

/**
 * @Date 2023-10-24
 * @Author qiu
 * 第八章：文件上传
 */
@RestController
@Slf4j
public class ProductContoroler {

    /**
     * 添加商品，同时带有上传的附件
     * @param productVO
     * @return
     */
    @PostMapping("/add")
    public ResultVO add(ProductVO productVO) throws IOException {
        ResultVO resultVO = new ResultVO();
        // 获取上传的路径(绝对路径)
        String uploadPath = "d://file/";

        // 拼接完整的上传路径
//        uploadPath += filename;

        log.info(uploadPath);
        // 根据路径构建一个上传的文件对象
        File uploadFile = new File(uploadPath);
        // 判断路径中的文件夹是否存在，不存在则创建
        if (!uploadFile.exists()) {
            // 将文件夹创建出来
            uploadFile.mkdirs();
        }
        // 获取上传的文件名
        MultipartFile[] file = productVO.getFile();
        for (MultipartFile multipartFile : file) {
            // 获取文件名
            String filename = multipartFile.getOriginalFilename();

            // 执行上传
            Path path = FileSystems.getDefault().getPath(uploadFile.getAbsolutePath(),filename);

            multipartFile.transferTo(path);
        }
        return resultVO;
    }


    /**
     * 文件下载
     * @param fileName
     * @return
     */
    @GetMapping("/download/{fileName}")
    public ResponseEntity<InputStreamResource> download(@PathVariable("fileName") String fileName) throws Exception {
        // 文件下载目录(也就是上传路径)
        String downloadPath = "d://file/" + fileName;
        // 构建一个文件输入流读取服务器上的文件
        FileInputStream fis = new FileInputStream(downloadPath);
        // 设置响应头，告诉浏览器响应流程
        HttpHeaders headers = new HttpHeaders();
        // 对文件名进行编码，防止响应头中出现乱码
        fileName = URLEncoder.encode(fileName,"UTF-8");
        // 设置头信息，将响应内容处理的方式设置为附件下载
        headers.setContentDispositionFormData("attachment",fileName);
        // 设置响应类型为流类型
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        //　创建　InputStreamResource 对象封装输入流，用于读取服务器文件
        InputStreamResource isr = new InputStreamResource(fis);
        // 创建 ResponseEntity 对象（封装 InputStreamResource,响应头，以及响应状态码）
        ResponseEntity<InputStreamResource> entity = new ResponseEntity<>(isr, headers,HttpStatus.CREATED);
        return entity;
    }


}
