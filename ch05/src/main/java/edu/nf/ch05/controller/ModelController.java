package edu.nf.ch05.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * @Date 2023-10-23
 * @Author qiu
 * 第五章：Model
 */
@Controller
public class ModelController {

    @GetMapping("/test1")
    public ModelAndView test1() {
        ModelAndView mav = new ModelAndView("index");
        // 设置请求作用域
        mav.addObject("name", "qiu");
        // 也可以将数据保存到 map 集合中，再设置列到 ModelAndView
        Map<String, Object> map = new HashMap<>();
        map.put("age", 21);
        // 再将 map 设置到 ModelAndView 中
        mav.addAllObjects(map);
        return mav;
    }

    /**
     * Spring 可以传入一个 Map 、Model 或者 ModelMap
     * 来保存用户需要转发的数据，这些数据一样会保存到请求
     * 作用域中
     * @param map
     * @return
     */
    @GetMapping("/test2")
    public ModelAndView test2(Map<String, Object> map) {
        map.put("name", "qiu");
        map.put("age", 21);

        return new ModelAndView("index");
    }

    /**
     * Model
     * @param model
     * @return
     */
    @GetMapping("/test22")
    public ModelAndView test22(Model model) {
        model.addAttribute("name", "qiu");
        model.addAttribute("age", 21);

        return new ModelAndView("index");
    }

}
