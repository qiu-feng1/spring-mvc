package edu.nf.ch09.exception;

/**
 * @Date 2023-10-27
 * @Author qiu
 * 自定义异常
 */
public class AuthException extends RuntimeException{

    /**
     * 异常状态码
     */
    private Integer erroeCode;

    public AuthException(Integer erroeCode,String message){
        super(message);
        this.erroeCode =erroeCode;
    }

    public Integer getErroeCode() {
        return erroeCode;
    }
}
