package edu.nf.ch09.entity;

import lombok.Data;

/**
 * @Date 2023-10-27
 * @Author qiu
 */
@Data
public class User {

    private String userName;
    private String password;

}
