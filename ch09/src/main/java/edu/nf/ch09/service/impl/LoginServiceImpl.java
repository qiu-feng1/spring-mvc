package edu.nf.ch09.service.impl;

import edu.nf.ch09.dao.UserDao;
import edu.nf.ch09.entity.User;
import edu.nf.ch09.exception.AuthException;
import edu.nf.ch09.service.LoginService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @Date 2023-10-27
 * @Author qiu
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class LoginServiceImpl implements LoginService {

    private final UserDao userDao;

    @Override
    public User auth(String userName, String password) {

        User user = userDao.getUser(userName);
        // 用户不为 Null 则校验密码
        if ( user != null ){
            if ( password.equals(user.getPassword())){
                return user;
            }
        }
        // 抛出业务异常
        throw new AuthException(10001,"账号密码错误");
    }

}
