package edu.nf.ch09.service;

import edu.nf.ch09.entity.User;

/**
 * @Date 2023-10-27
 * @Author qiu
 */
public interface LoginService {

    /**
     * 验证用户
     * @param userName
     * @param password
     * @return
     */
    User auth(String userName,String password);

}
