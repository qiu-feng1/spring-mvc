package edu.nf.ch09.dao.impl;

import edu.nf.ch09.dao.UserDao;
import edu.nf.ch09.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

/**
 * @Date 2023-10-27
 * @Author qiu
 */
@Repository
@Slf4j
public class UserDaoImpl implements UserDao {
    @Override
    public User getUser(String userName) {
        log.info("select * from user ");
        User user = new User();
        user.setUserName("qiu");
        user.setPassword("88888888");
        return user;
    }
}
