package edu.nf.ch09.dao;

import edu.nf.ch09.entity.User;

/**
 * @Date 2023-10-27
 * @Author qiu
 */
public interface UserDao {


    User getUser(String userName);

}
