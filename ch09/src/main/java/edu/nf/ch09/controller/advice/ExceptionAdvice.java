package edu.nf.ch09.controller.advice;

import edu.nf.ch09.controller.vo.ResultVO;
import edu.nf.ch09.exception.AuthException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @Date 2023-10-27
 * @Author qiu
 * 定义一个全局异常处理类（类似一个切面）
 * 这个类中定义所有的异常处理方法，也可以
 * 理解为全局异常通知
 */
//@ControllerAdvice (对应 @Controller 注解的类)

// (对应 @RestController 注解的类)
// value 屬性 controller 包下所有类都需要捕获异常
@RestControllerAdvice("edu.nf.ch09.controller")

@Slf4j
public class ExceptionAdvice {

    /**
     * 全局异常处理，处理登录业务异常
     * @ExceptionHandler：注解标注的方法专门用于处理请求方式产生的异常。
     * value:属性指定要处理的异常类型
     * 注意：这个局部异常的范围只在当前的 Controller 中有效，也就是说
     * 每个 controller 都有自己专门的 handlerException
     * @param e
     * @return
     */
    @ExceptionHandler(AuthException.class)
    public ResultVO handlerAuthException(AuthException e){
        // 验证未通过则创建提示信息
        ResultVO resultVO = new ResultVO();
        resultVO.setCode(e.getErroeCode());
        resultVO.setMessage(e.getMessage());
        return resultVO;
    }


    /**
     * 全局的异常处理（处理非业务异常，如：数据库异常）
     * @return
     */
    @ExceptionHandler(RuntimeException.class)
    public ResultVO handlerRunTimeException(RuntimeException e ){
        // 捕获其他非业务异常(也就是服务器内部错误异常)
        // 记录异常日志
        log.error(e.getMessage());
        // 验证未通过则创建提示信息
        ResultVO resultVO = new ResultVO();
        resultVO.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        resultVO.setMessage("服务器内部错误，请稍候重试");
        return resultVO;
    }

}
