package edu.nf.ch09.controller;

import edu.nf.ch09.controller.vo.ResultVO;
import edu.nf.ch09.entity.User;
import edu.nf.ch09.service.LoginService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * @Date 2023-10-27
 * @Author qiu
 */
@RestController
@RequiredArgsConstructor
@Slf4j
public class UserController {

    private final LoginService loginService;


//    @PostMapping("/login")
//    public ResultVO login(String userName, String password, HttpSession session){
//
//        try{
//            User user = loginService.auth(userName, password);
//            // 将 user 保存到会话中
//            user.setUserName(userName);
//            user.setPassword(password);
//            session.setAttribute("user",user);
//
//            return new ResultVO();
//        } catch (AuthException e){
//            // 验证未通过则创建提示信息
//            ResultVO resultVO = new ResultVO();
//            resultVO.setCode(e.getErroeCode());
//            resultVO.setMessage(e.getMessage());
//            return resultVO;
//            // 捕获其他非业务异常(也就是服务器内部错误异常)
//        } catch (RuntimeException e){
//            // 记录异常日志
//            log.error(e.getMessage());
//            // 验证未通过则创建提示信息
//            ResultVO resultVO = new ResultVO();
//            resultVO.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
//            resultVO.setMessage("服务器内部错误，请稍候重试");
//            return resultVO;
//        }
//
//    }



//    /**
//     * 局部异常处理，处理登录业务异常
//     * @ExceptionHandler：注解标注的方法专门用于处理请求方式产生的异常。
//     * value:属性指定要处理的异常类型
//     * 注意：这个局部异常的范围只在当前的 Controller 中有效，也就是说
//     * 每个 controller 都有自己专门的 handlerException
//     * @param e
//     * @return
//     */
//    @ExceptionHandler(AuthException.class)
//    public ResultVO handlerAuthException(AuthException e){
//        // 验证未通过则创建提示信息
//        ResultVO resultVO = new ResultVO();
//        resultVO.setCode(e.getErroeCode());
//        resultVO.setMessage(e.getMessage());
//        return resultVO;
//    }


    @PostMapping("/login")
    public ResultVO login(String userName, String password, HttpSession session){

        User user = loginService.auth(userName, password);
        // 将 user 保存到会话中
        user.setUserName(userName);
        user.setPassword(password);
        session.setAttribute("user",user);

        return new ResultVO();

    }


}
