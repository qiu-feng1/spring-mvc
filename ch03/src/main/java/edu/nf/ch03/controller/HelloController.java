package edu.nf.ch03.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @Date 2023-10-20
 * @Author qiu
 *
 * 第三章：处理静态资源
 */
@Controller
public class HelloController {

    @GetMapping("/hello")
    public ModelAndView hello(){
        return new ModelAndView("index");
    }

}
