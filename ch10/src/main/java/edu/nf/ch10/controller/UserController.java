package edu.nf.ch10.controller;

import edu.nf.ch10.controller.vo.ResultVO;
import edu.nf.ch10.entity.User;
import edu.nf.ch10.service.LoginService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * @Date 2023-10-27
 * @Author qiu
 */
@RestController
@RequiredArgsConstructor
public class UserController {

    private final LoginService service;

    @PostMapping("/auth")
    public ResultVO login(String userName, String password, HttpSession session){
        User auth = service.auth(userName, password);
        session.setAttribute("user",auth);
        return new ResultVO();
    }

}
