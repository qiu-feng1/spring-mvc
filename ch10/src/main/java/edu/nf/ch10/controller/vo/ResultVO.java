package edu.nf.ch10.controller.vo;

import lombok.Data;
import org.springframework.http.HttpStatus;

/**
 * @Date 2023-10-24
 * @Author qiu
 */
@Data
public class ResultVO<T> {

    private Integer code = HttpStatus.OK.value();
    private String message;
    private T data;

}
