package edu.nf.ch10.interceptor;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.nf.ch10.controller.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @Date 2023-10-27
 * @Author qiu
 * 认证拦截器
 * 拦截所有的请求，如果未登录则返回 401（未登录，未认证） 状态码
 */
@Slf4j
public class AuthInterceptor implements HandlerInterceptor {

    /**
     * 在调用 controller 的请求方法之前执行
     * 如果此方法返回 false ，则请求不会继续往下执行
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("执行 preHandle 方法");
        HttpSession session = request.getSession();
        // 如果 session 为 null 表示用户未登录
        if ( session.getAttribute("user") == null ){
            ResultVO resultVO = new ResultVO();
            // 设置 401 状态码
            resultVO.setCode(HttpStatus.UNAUTHORIZED.value());
            response.setContentType("application/json;charset=utf-8");
            String json = new ObjectMapper().writeValueAsString(resultVO);
            response.getWriter().println(json);
            return false;
        }
        return true;
    }

    /**
     * 在调用 controller 方法之后，返回之前执行
     * @param request
     * @param response
     * @param handler
     * @param modelAndView
     * @throws Exception
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        log.info("执行 postHandle 方法");
    }

    /**
     * 调用 controller 方法并返回之后执行
     * （注意：只有在 preHandle 返回 TRUE ，才会执行）
     * @param request
     * @param response
     * @param handler
     * @param ex
     * @throws Exception
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        log.info("执行 afterCompletion 方法");
    }
}
