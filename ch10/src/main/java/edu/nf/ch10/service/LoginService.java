package edu.nf.ch10.service;

import edu.nf.ch10.entity.User;

/**
 * @Date 2023-10-27
 * @Author qiu
 */
public interface LoginService {

    /**
     * 登录认证
     * @param userName
     * @param password
     * @return
     */
    User auth(String userName,String password);

}
