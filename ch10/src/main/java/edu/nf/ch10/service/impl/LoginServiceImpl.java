package edu.nf.ch10.service.impl;

import edu.nf.ch10.entity.User;
import edu.nf.ch10.service.LoginService;
import org.springframework.stereotype.Service;

/**
 * @Date 2023-10-27
 * @Author qiu
 */
@Service
public class LoginServiceImpl implements LoginService {
    @Override
    public User auth(String userName, String password) {

        if ("qiu".equals(userName) && "123".equals(password)){
            User user = new User();
            user.setUserName(userName);
            user.setPassword(password);
            return user;
        }
        throw new RuntimeException("账号密码错误");
    }
}
