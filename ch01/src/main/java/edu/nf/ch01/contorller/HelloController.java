package edu.nf.ch01.contorller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @Date 2023-10-19
 * @Author qiu
 * 控制层使用 @controller 注解标识
 */
@Controller
@Slf4j
public class HelloController {

    @RequestMapping("/hello")
    public ModelAndView hello() {
        log.info("Hello word");
        // 响应视图,JSP 视图解析器都是基于转发的机制
        ModelAndView index = new ModelAndView("index");
        return index;
    }

}
