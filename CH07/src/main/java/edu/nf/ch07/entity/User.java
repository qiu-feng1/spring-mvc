package edu.nf.ch07.entity;

import lombok.Data;

import javax.validation.constraints.*;
import java.util.Date;

/**
 * @Date 2023-10-24
 * @Author qiu
 */
@Data
public class User {

    /**
     * 验证空字符串
     */
    @NotEmpty(message = "{user.userName.notEmpty}")
    private String userName;

    /**
     * 验证空值
     */
    @NotNull(message = "{user.age.notNull}")
    /**
     * 最小值范围
     */
    @Min(value = 18,message = "{user.age.min}")
    private Integer age;


    @NotNull(message = "{user.birth.notNull}")
    //@DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birth;

    @NotEmpty(message = "{user.email.notEmpty}")
    @Email(message = "{user.email.legal}")
    private String email;

}
