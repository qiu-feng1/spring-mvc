package edu.nf.ch07.controller.vo;

import lombok.Data;
import org.springframework.http.HttpStatus;

/**
 * @Date 2023-10-24
 * @Author qiu
 */
@Data
public class ResultVO<T> {

    // 响应状态码，默认 200
    private Integer code = HttpStatus.OK.value();
    private String message;
    private T data;

}
