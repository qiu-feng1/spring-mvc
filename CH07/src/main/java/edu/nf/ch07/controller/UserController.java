package edu.nf.ch07.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.nf.ch07.controller.vo.ResultVO;
import edu.nf.ch07.entity.User;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.swing.text.Keymap;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * @Date 2023-10-24
 * @Author qiu
 * 第七章：表单验证
 */
@RestController
public class UserController {

    /**
     * 注册日期格式化器
     * @param binder
     */
    @InitBinder
    public void regFormatter(WebDataBinder binder) {
        binder.addCustomFormatter(
                new DateFormatter("yyyy-MM-dd"));
    }

    /**
     * @Valid注解标注的参数实体要参与Bean验证
     * @param user
     * @param result 这个结果集存放了验证信息，如果校验未通过
     *               就从这个结果集中获取验证信息
     * @return
     */
    @PostMapping("/add")
    public ResultVO add(@Valid User user, BindingResult result) throws JsonProcessingException {
        ResultVO vo = new ResultVO();
        //先判断是否校验通过,如果存在错误消息则表示未通过
        if(result.hasErrors()) {
            //创建一个map来保存这些错误消息
            Map<String, String> errors = new HashMap<>();
            //循环遍历所有的错误信息
            result.getFieldErrors().forEach(fieldError -> {
                //以字段名作为key，错误信息作为value保存到map中
                errors.put(fieldError.getField(),
                        fieldError.getDefaultMessage());
            });
            //设置响应状态码
            vo.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            //将map序列化成json字符串
            String messages = new ObjectMapper().writeValueAsString(errors);
            vo.setMessage(messages);
            //直接将errors保存到data中
            vo.setData(errors);
            return vo;
        }
        return vo;
    }

}
