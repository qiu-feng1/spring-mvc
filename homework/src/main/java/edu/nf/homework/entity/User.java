package edu.nf.homework.entity;

import lombok.Data;

/**
 * @Date 2023-10-24
 * @Author qiu
 */
@Data
public class User {

    private String userName;
    private String pass;

}
