package edu.nf.homework;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @Date 2023-10-25
 * @Author qiu
 */
@WebServlet("/home")
public class HomeServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 从会话找到用户信息
        HttpSession session = req.getSession();
        String userName = (String) session.getAttribute("userName");

        if (!userName.equals("")){
            req.getRequestDispatcher("/jsp/login.jsp").forward(req,resp);
        } else{
            resp.sendRedirect("/jsp/home.jsp");
        }

    }
}
