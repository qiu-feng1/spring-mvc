package edu.nf.homework;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @Date 2023-10-25
 * @Author qiu
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("userName");
        String pwd = req.getParameter("pass");

        // 保存到会话中
        HttpSession session = req.getSession();
        session.setAttribute("userName", name);

        if (!name.equals("")){
            req.getRequestDispatcher("/jsp/home.jsp").forward(req,resp);
        }

    }
}
