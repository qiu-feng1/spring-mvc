package edu.nf.homework.controller;

import com.fasterxml.jackson.annotation.JsonCreator;
import edu.nf.homework.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @Date 2023-10-24
 * @Author qiu
 */
@RestController
@Slf4j
public class UserController {

    @PostMapping("/login")
    public ModelAndView login(HttpServletRequest request, HttpSession session){
        String userName = request.getParameter("userName");
        String password = request.getParameter("pass");
        log.info("账号：" + userName);
        log.info("密码：" + password);

        ModelAndView modelAndView  ;
        if ( userName!= "null"){
            session.setAttribute("userName",userName);
            modelAndView = new ModelAndView("home");
        } else{
           modelAndView = new ModelAndView("login");
        }

        return modelAndView;
    }

    @GetMapping("/home")
    public ModelAndView home(HttpSession httpSession){
        // 从会话中获取用户名
        String getUserName = (String) httpSession.getAttribute("userName");

        ModelAndView modelAndView;
        if ( getUserName.equals("") && getUserName.equals(null)){
            modelAndView = new ModelAndView("home");
        } else{
            modelAndView = new ModelAndView("login");
        }
        return modelAndView;
    }

}
