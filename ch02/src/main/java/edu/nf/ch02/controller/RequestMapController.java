package edu.nf.ch02.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * @Date 2023-10-19
 * @Author qiu
 * @RequestMapping:可以声明在类上，用作命名空间 可以区别多个相同的子请求
 * 第二章：请求方式
 */
@Controller
public class RequestMapController {

    @RequestMapping(value = "/getUser", method = RequestMethod.GET)
    public ModelAndView getUser() {
        return new ModelAndView("index");
    }

    @RequestMapping(value = "/login", method = {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView login() {
        return new ModelAndView("index");
    }

    // 从 springmvc 4.0 开始，提供了新的子注解来替换 @RequestMapping

    /**
     * @return
     * @GetMapping()：只用于 get 请求
     */
    @GetMapping("/getUser2")
    public ModelAndView getUser2() {
        return new ModelAndView("index");
    }

    /**
     * @PostMapping() : 只用于 post 请求
     * @return
     */
    @PostMapping("/login2")
    public ModelAndView login2() {
        return new ModelAndView("index");
    }

}
