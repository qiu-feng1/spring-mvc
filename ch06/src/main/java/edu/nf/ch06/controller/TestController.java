package edu.nf.ch06.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * @Date 2023-10-23
 * @Author qiu
 * 第六章：视图
 */
@Controller
@Slf4j
public class TestController {

    /**
     * 转发视图
     * @return
     */
    @GetMapping("/forward1")
    public ModelAndView forward1(){
        return new ModelAndView("index");
    }

    /**
     * 转发视图 2
     * @return
     */
    @GetMapping("/forward2")
    public String forward2(){
        log.info("转发视图");
        return "index";
    }

    /**
     * 转发的 url 必须使用 forward 前缀
     * 注意：转发视图是通过 InternalResourceViewResolver
     * 解析器来完成的，而转发请求的 url 是一个地址，并非视图
     * 因此需要加上 forward：前缀，并且 url 的路径必须写完整
     * @return
     */
    @GetMapping("/forward3")
    public String forward3(){
        return "forward:forward2";
    }

    /**
     * 如果需要 重定向一个视图或者请求地址
     * 可以结合”redirect:“这个前缀
     * 并且重定向的地址和视图名称 必须是完整的 url 路径
     * 因为重定向是不会走内部资源视图解析器的
     *
     * 注意：重定向不能传递请求数据，需要通过其他方式传递参数
     * @return
     */
    @GetMapping("/redirect1")
    public String redirect1(Model model){
        model.addAttribute("name","qiu");
        return "redirect:redirect.jsp";
    }

    /**
     * RedirectAttributes 是 spring MVC3.1 版本提供的一个类
     * 主要用于在重定向时可以重写 url ，在重定向的 url 后面加入
     * 参数，这样就可以变相在重定向的资源中获取相关的参数信息
     * @param redirectAttributes
     * @return
     */
    @GetMapping("/redirect2")
    public String redirect2(RedirectAttributes redirectAttributes){
        redirectAttributes.addAttribute("name","qiu");
        return "redirect:redirect.jsp";
    }



}
