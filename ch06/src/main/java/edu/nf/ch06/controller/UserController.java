package edu.nf.ch06.controller;

import edu.nf.ch06.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @Date 2023-10-23
 * @Author qiu
 */
//@Controller
@Slf4j

/**
 * @RestController:是在 spring4.0后面加入的一个注解，
 * 同样用于标注为控制器的组件，如果当前 controller 中所有请求
 * 方法都需要使用 @ResponseBody 注解来响应，，那么就可以使用
 * 它标注在类上，而不需要在每一个方法上标注 @RestController
 */
@RestController
public class UserController {

    @GetMapping("/user")
    /**
     * 使用 @ResponseBody 注解，表示将方法返回值
     * 以输出流的方式写回客户端，这样 springmvc
     * 就会将序列化好的 json 的数据放入响应体并写回
     */
//    @ResponseBody
    public User getUSer(){
        User user = new User();
        user.setUserName("user1");
        return user;
    }

    /**
     * 当前端提交的是一个 JSON 字符串时，
     * 此时要使用 @RequestBody 注解来映射，
     * 这样 springmvc 就会将 json 字符串
     * 反序列化为实体对象
     * @param user
     * @return
     */
    @PostMapping("/add")
    public String add(@RequestBody User user){
        log.info(user.getUserName());
        return "success";
    }

}
