package edu.nf.ch06.entity;

import lombok.Data;

/**
 * @Date 2023-10-23
 * @Author qiu
 */
@Data
public class User {

    private String userName;

}
