package edu.nf.ch11.controller;

import edu.nf.ch11.entity.User;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @Date 2023-10-30
 * @Author qiu
 */
@RestController
public class UserController {

    @PostMapping("/add")
    public Integer add(@Valid User user){
        return HttpStatus.OK.value();
    }

}
