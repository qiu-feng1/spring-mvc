package edu.nf.ch11.config;

import edu.nf.ch11.interceptor.LoginInterceptor;
import org.hibernate.validator.HibernateValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * @Date 2023-10-30
 * @Author qiu
 * springmvc 的配置类，用于取代 dispatcher-servlet.xml
 */
// 声明为配置类
@Configuration
// 启用包扫描
@ComponentScan(basePackages = "edu.nf.ch11")
// 启用 mvc 注解驱动(等效于<mvc:annotation-driven/>)
@EnableWebMvc
// 实现 WebMvcConfigurer 接口用于覆盖默认的配置
public class MvcConfig implements WebMvcConfigurer {
    /**
     *  配置类支持依赖注入，配置类也是容器管理的
     */
//    @Autowired
//    private LoginInterceptor loginInterceptor;

    /**
     * 静态资源处理方式一：使用容器默认的 servlet 来处理
     * 注意：重写 WebMvcConfig 接口方法时不需要使用 @Bean
     *      注解来装配
     */
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        // enable() 方法表示启用容器默认 servlet 处理静态资源
        configurer.enable();
    }

    /**
     * 装配 Bean 验证器
     */
    @Override
    public Validator getValidator() {
        LocalValidatorFactoryBean factoryBean = new LocalValidatorFactoryBean();
        // 使用 Hibernate 框架提供的 Bean 验证器
        factoryBean.setProviderClass(HibernateValidator.class);
        // 指定资源文件
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:message");
        messageSource.setDefaultEncoding("UTF-8");
        // 装配给 factoryBean
        factoryBean.setValidationMessageSource(messageSource);
        return factoryBean;
    }

    /**
     * 装配 commons-upload 上传解析器
     */
    @Bean
    public CommonsMultipartResolver multipartResolver(){
        CommonsMultipartResolver resolver = new CommonsMultipartResolver();
        // 设置文件上传的大小
        resolver.setMaxUploadSize(104857600);
        // 设置编码
        resolver.setDefaultEncoding("UTF-8");
        return resolver;
    }

    /**
     * 装配拦截器
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/static/login.html",
                                     "/static/js/**",
                                     "/static/css/**",
                                     "/login");
    }

    /**
     * 跨域配置
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedMethods("*")
                .allowedHeaders("*")
                .exposedHeaders("*");
                // 跨域时时候允许传递 cookie,默认是不允许的
                //.allowCredentials(true);
    }


    /**
     * 静态资源处理方式二：由 springmvc 处理静态资源
     * 页面上以 page 开头的页面都放到 static 管理
     * @param registry
     */
//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry.addResourceHandler("/page/**")
//                .addResourceLocations("/static/");
//    }



    /**
     * 配置内部资源视图解析器
     */
    @Bean
    public InternalResourceViewResolver viewResolver(){
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();

        viewResolver.setPrefix("/WEN-INF/jsp/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;

    }

}
