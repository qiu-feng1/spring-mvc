package edu.nf.ch11.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * @Date 2023-10-30
 * @Author qiu
 * webConfig 配置类，用于取代 web.xml
 * 在继承的父类中已经帮我们创建了 dispatcherServlet 并初始化
 * 只需要在重写的 getServletMappings 方法中指定映射的地址即可
 * 相当于 web.xml  中的 <url-pattern>/</url-pattern>
 */
public class WebConfig extends AbstractAnnotationConfigDispatcherServletInitializer {

    /**
     * 加载主配置类
     * @return
     */
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[0];
    }

    /**
     * 加载 MVC 的配置类
     * @return
     */
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{MvcConfig.class};
    }

    /**
     * 给 dispatcherServlet 配置 url-pattern
     * @return
     */
    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

}
