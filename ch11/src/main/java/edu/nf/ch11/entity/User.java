package edu.nf.ch11.entity;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @Date 2023-10-30
 * @Author qiu
 */
@Data
public class User {

    @NotEmpty(message = "{user.userName}")
    private String userName;
    @NotEmpty(message = "{user.password}")
    private String password;

}
